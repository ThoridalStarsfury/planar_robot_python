from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='planar_robot_python',
            executable='trajectory_generator',
            name='trajectory_generator'
        ),
        Node(
            package='planar_robot_python',
            executable='controller',
            name='controller'
        ),
        Node(
            package='planar_robot_python',
            executable='kinematic_model',
            name='kinematic_model',
        ),
        Node(
            package='planar_robot_python',
            executable='simulator',
            name='simulator',
        ),
        Node(
            package='planar_robot_python',
            executable='disturbance',
            name='disturbance',
        ),
        Node(
            package='planar_robot_python',
            executable='high_level_manager',
            name='high_level_manager',
        ),
        Node(
            package='tf2_ros',
            executable='static_transform_publisher',
            name='static_transform_publisher',
            output='screen',
            arguments= ['0','0','0','0','0','0','axis','odom']

        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{'publish_frequency':30.0}],
            arguments=['/home/thoridal/Documents/Programmation Robotique/src/planar_robot_python/urdf/planar_robot.urdf.xml']
        )
    ])