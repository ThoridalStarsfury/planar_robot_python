import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np
from math import *

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')

        self.publisher_     = self.create_publisher(JointState, 'qdot_disturbance', 10)
        self.declare_parameter('amplitude', 1)
        self.declare_parameter('w', 10*pi)
        self.t                  = 0
        self.sampling_period    = 1e-2
        self.disturbance        = np.zeros(2)


        self.timer              = self.create_timer(self.sampling_period, self.timer_callback)


    def timer_callback(self):
        self.t                  = self.t + self.sampling_period

        A = self.get_parameter('amplitude').value
        w = self.get_parameter('w').value
        self.disturbance[0] = A*sin(w*self.t)
        self.disturbance[1] = self.disturbance[0]

        qdot_disturbance = JointState()
        qdot_disturbance.velocity = self.disturbance

        self.publisher_.publish(qdot_disturbance)

def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()