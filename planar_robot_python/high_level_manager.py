import rclpy
import os
from rclpy.node import Node
from custom_messages.msg import RefPosition
from custom_messages.msg import KinData
from std_msgs.msg import Bool
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class HighLevelManager(Node):
    def __init__(self):
        super().__init__('high_level_manager')
        self.motion_data    = np.loadtxt(open(os.getcwd() + "/src/planar_robot_python/trajectories/test.csv", "rb"), delimiter=",", skiprows=1)        
        self.publisher_     = self.create_publisher(
            RefPosition, 
            'ref_position', 
            10)        
        self.subs_kin_data = self.create_subscription(
            KinData,
            'kin_data',
            self.listener_callback_kin_data,
            10)
        self.subs_launch = self.create_subscription(
            Bool,
            'launch',
            self.listener_callback_launch,
            10)
        self.subs_kin_data
        self.subs_launch
        self.initialized        = False
        self.running            = False
        self.pold               = np.zeros(2)
        self.pnext              = np.zeros(2)
        self.i                  = 0
        self.N                  = self.motion_data.shape[0]

    def listener_callback_kin_data(self, kin_data):
        if self.initialized == False:
            self.initialized    = True
            self.pold           = np.array([kin_data.x,kin_data.y])
            self.pnext          = self.pold            
            ref_position        = RefPosition()
            ref_position.x      = self.pnext[0]
            ref_position.y      = self.pnext[1]
            ref_position.deltat = 0.0
            self.publisher_.publish(ref_position)            
        elif self.running and self.i < self.N:
            p                   = np.array([kin_data.x,kin_data.y])
            if np.linalg.norm(p-self.pnext) < 1e-3: 
                # np.linalg.norm est pour calculer la distance de "p" et "self.pnext" dans l'Espace euclidien
                self.pold           = self.pnext
                self.pnext          = self.motion_data[self.i,0:2]
                self.deltat         = self.motion_data[self.i,2]
                ref_position        = RefPosition()
                ref_position.x      = self.pnext[0]
                ref_position.y      = self.pnext[1]
                ref_position.deltat = self.deltat
                self.i              = self.i + 1
                self.publisher_.publish(ref_position)

    def listener_callback_launch(self, launch_in):
        if not self.running and launch_in:
            self.running        = True
            self.pold           = self.pnext
            self.pnext          = self.motion_data[self.i,0:2]
            self.deltat         = self.motion_data[self.i,2]
            ref_position        = RefPosition()
            ref_position.x      = self.pnext[0]
            ref_position.y      = self.pnext[1]
            ref_position.deltat = self.deltat
            self.i              = self.i + 1
            self.publisher_.publish(ref_position)

def main(args=None):
    rclpy.init(args=args)
    high_level_manager = HighLevelManager()
    rclpy.spin(high_level_manager)
    high_level_manager.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()