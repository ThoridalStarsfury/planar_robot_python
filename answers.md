# P2 answer

## Merge Comments

Comment on the differences between your `p1_results` and the ones given for this practical.

La initialisation est un peu differente, il n'a pas besoin de calcule la position du robot a la initialisation.

## Result Comments

Are your results identical to the ones demanded in **practical 2**? If not, why?

J'ai calcule deux fois la position dans la initialisation, et je n'ai pas utilise "get_logger().info"

# P3 answer

## Merge Comments

Je n'avais pas bien compris le point "{x: 1.0, y: 1.0, jacobian:[0.0,0.0,0.0,0.0]}" dans le p3.sh avant, en fait il s'agit de verifier que le robot a atteint l'emplacement desire, si le robot arrive la position ou nous voulons qu'il soit, nous donnons la prochaine position. Dans mon code, je n'ai pas reussi a faire cette partie.
Le code marche bien

## Result Comments

Le code marche bien

# P4 answer

## Merge Comments
code donne par le prof

## Result Comments
code donne par le prof

# P5 answer

## Merge Comments


Pour le controller : Je n'ai pas bien enregistre la valeur de Jacobienne, je n'ai pas realise self.running et self.initialized, et je n'ai pas bien publier "joint_state"

Pour le simulator : je n'ai pas bien publier "joint_state", je n'ai pas utilise "self.get_clock().now()" et je n'ai pas bien initialise la valeur de q, j'ai mis "self.q_prev = np.zeros(2)" en fait il est  "self.q = np.array([0, np.pi/2])"

## Result Comments

Le code marche bien

# P6 answer

## Merge Comments

le code marche bien, le robot arrive des position ou on veut qu'il soit, les courbes sont bien affichees, il n'y a qu'un probleme, c'est difficile de distinguer l'axe des abscisses, l'axe des ordonnees l'axe z et leur direction positive de ce systeme de coordonnees.

## Result Comments

Attente du Merge prof pour commenter